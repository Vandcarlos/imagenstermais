// Esse algoritmo retorna as temperaturas para um determinado binário

#include <iostream>
#include <cstdlib>
#include <fstream>
#include <cmath>

using namespace std;

// Variáveis
// tempMin e TempMax são as temperaturas mínimas e máximas que serão abrangidas
// rangeMin e rangeMax são as faixas que se quer imprmir os valores, deixar igual a tempMin e tempMax para todos
// incremento é a quantidade de temperatura que muda por bits
// temp é a temeperatura para uma quantidade de bits
// bits é quantos bits irá se trabalhar
// combinacoes é a quantidade de combinações os bits permitem

int combinacaoEmBinario(int bits);

int main(int argc, char const *argv[]) {
  int i;
  int tempMin = -40, tempMax = 2000;
  int rangeMin = -10, rangeMax = 150;
  int bits = 16, combinacoes;
  float incremento;
  float temp;

  combinacoes = combinacaoEmBinario(bits);
  incremento = ((float)abs(tempMin) + abs(tempMax) )/ combinacoes;

  ofstream myfile;
  myfile.open("temperaturas.txt");

  for(i = 0; i < combinacoes; i = i + 10)
  {
    temp = tempMin + (i * incremento);
    if(temp > rangeMin && temp < rangeMax)
    {
      cout << "Temperatura = " << temp << "\t -- ";
      cout << "Bits em Decimal = " << i << endl;

      myfile << "Temperatura = " << temp << "\t -- ";
      myfile << "Bits em Decimal = " << i << endl;
    }
  }

  myfile.close();

  return 0;
}

int combinacaoEmBinario(int bits)
{
  int i, combinacoes = 0;

  for(i = 0; i < bits; i++)
  {
    combinacoes += (int)pow(2, i);
  }

  return combinacoes;
}
